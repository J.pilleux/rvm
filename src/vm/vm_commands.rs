use std::process::Command;

use crate::{
    components::{command_output::CommandOutput, menus::VMActionsMenus},
    vm::vm_action::VmAction,
};

pub fn get_vm_actions() -> Vec<VmAction<'static>> {
    vec![
        VmAction::new(
            "Start",
            "Start the selected VM",
            &run_vm,
            VMActionsMenus::Main,
        ),
        VmAction::new(
            "Stop",
            "Stop the selected VM",
            &stop_vm,
            VMActionsMenus::Main,
        ),
        VmAction::new(
            "Start headless",
            "Start the selected VM without launching the display",
            &start_vm_headless,
            VMActionsMenus::Main,
        ),
        VmAction::new(
            "Remove",
            "Remove the selected VM",
            &remove_vm,
            VMActionsMenus::Remove,
        ),
        VmAction::new(
            "Clone",
            "Clone the selected VM",
            &clone_vm,
            VMActionsMenus::Clone,
        ),
        VmAction::new(
            "Snapshots",
            "Snapshot manipulation menu",
            &dummy_command,
            VMActionsMenus::Snapshots,
        ),
    ]
}

fn dummy_command(_: &str, _: &str) -> CommandOutput {
    CommandOutput::from_str("", "")
}

fn run_vm(vm_name: &str, _: &str) -> CommandOutput {
    let output = Command::new("VBoxManage")
        .arg("startvm")
        .arg(vm_name)
        .output();

    match output {
        Ok(out) => CommandOutput::new(&out),
        Err(_) => CommandOutput::error_from_str(&format!("Cannot start vm « {} »", vm_name)),
    }
}

fn stop_vm(vm_name: &str, _: &str) -> CommandOutput {
    let output = Command::new("VBoxManage")
        .arg("controlvm")
        .arg(vm_name)
        .arg("poweroff")
        .output();

    match output {
        Ok(out) => CommandOutput::new(&out),
        Err(_) => CommandOutput::error_from_str(&format!("Cannot stop vm « {} »", vm_name)),
    }
}

fn start_vm_headless(vm_name: &str, _: &str) -> CommandOutput {
    let output = Command::new("VBoxManage")
        .arg("startvm")
        .arg(vm_name)
        .arg("--type")
        .arg("headless")
        .output();

    match output {
        Ok(out) => CommandOutput::new(&out),
        Err(_) => {
            CommandOutput::error_from_str(&format!("Cannot start without interface vm « {} »", vm_name))
        }
    }
}

fn clone_vm(vm_name: &str, new_name: &str) -> CommandOutput {
    const VM_TMP_PATH: &str = "/tmp/__vm_clone_tmp.ova";

    let export_output = Command::new("VBoxManage")
        .arg("export")
        .arg(vm_name)
        .arg("--output")
        .arg(VM_TMP_PATH)
        .status();

    if let Err(_) = export_output {
        return CommandOutput::error_from_str(&format!("Cannot export vm « {} »", vm_name));
    }

    let import_output = Command::new("VBoxManage")
        .arg("import")
        .arg(VM_TMP_PATH)
        .arg("--vsys")
        .arg("0")
        .arg("--vmname")
        .arg(new_name)
        .status();

    if let Err(_) = import_output {
        return CommandOutput::error_from_str(&format!("Cannot import new vm « {} »", new_name));
    }

    let modifyvm_output = Command::new("VBoxManage")
        .arg("modifyvm")
        .arg(new_name)
        .arg("--macaddress1")
        .arg("auto")
        .status();

    if let Err(_) = modifyvm_output {
        return CommandOutput::error_from_str(&format!(
            "Cannot change mac address for vm « {} »",
            new_name
        ));
    }

    let remove_output = std::fs::remove_file(VM_TMP_PATH);

    match remove_output {
        Ok(_) => CommandOutput::from_str(
            &format!("Successfully cloned « {} » into « {} »", vm_name, new_name),
            "",
        ),
        Err(_) => CommandOutput::error_from_str(&format!(
            "Cannot delete temporary export « {} », the vm « {} » should still be working",
            VM_TMP_PATH, new_name
        )),
    }
}

fn remove_vm(vm_name: &str, _: &str) -> CommandOutput {
    let output = Command::new("VBoxManage")
        .arg("unregistervm")
        .arg("--delete")
        .arg(vm_name)
        .output();

    match output {
        Ok(out) => CommandOutput::new(&out),
        Err(_) => CommandOutput::error_from_str(&format!("stop vm « {} »", vm_name)),
    }
}
