use crate::components::{command_output::CommandOutput, menus::VMActionsMenus};

pub trait Action<'a> {
    fn new(name: String, description: String, action: &'a dyn Fn(&str, &str) -> CommandOutput, menu: VMActionsMenus) -> Self;
    fn get_name(&self) -> &String;
    fn get_description(&self) -> &String;
    fn get_action(&self) -> &'a dyn Fn(&str, &str) -> CommandOutput;
    fn get_menu(&self) -> &VMActionsMenus;
}
