use regex::Regex;
use std::process::Command;
use super::virtual_machine::Vm;

pub fn get_vm_list() -> Vec<Vm> {
    let mut vm_list: Vec<Vm> = Vec::new();

    match get_vm_names() {
        Ok(list) => {
            for vm_name in list {
                let snapshot_lists = get_snapshot_lists(&vm_name);
                let vm = Vm::new(vm_name.as_str(), snapshot_lists.0, snapshot_lists.1);
                vm_list.push(vm);
            }
        }
        Err(..) => vm_list.push(Vm::new(
            "NO VM FOUND",
            vec!["NO SNAP FOUND".to_string()],
            vec!["NO SNAP FOUND".to_string()],
        )),
    };

    vm_list
}

fn get_vm_names() -> Result<Vec<String>, String> {
    let output = Command::new("VBoxManage")
        .arg("list")
        .arg("--sorted")
        .arg("vms")
        .output();

    match output {
        Ok(out) => {
            let command_output = String::from_utf8_lossy(&out.stdout);
            let machines: Vec<String> = command_output
                .split("\n")
                .map(get_machine_name)
                .filter(|s| s != "")
                .collect();

            Ok(machines)
        }
        Err(..) => Err(String::from("ERROR: Cannot get vm list")),
    }
}

fn get_machine_name(line: &str) -> String {
    let split: Vec<&str> = line.split("\" ").collect();
    String::from(split[0].replace("\"", ""))
}

fn format_snapshot_name(line: String) -> String {
    let saved = String::from(&line);
    let parts: Vec<&str> = line.split(" (").collect();
    let name = parts[0].replace("Name:", "-");
    if saved.trim().ends_with("*") {
        format!("{} «", name)
    } else {
        String::from(name)
    }
}

fn extract_snapshot_name(line: String) -> String {
    let parts: Vec<&str> = line.split(" (").collect();
    let name_parts: Vec<String> = parts[0].split("Name: ").map(String::from).collect();

    if name_parts.len() > 1 {
        name_parts[1].trim().to_string()
    } else {
        line
    }
}

pub fn get_snapshot_lists(vm_name: &str) -> (Vec<String>, Vec<String>) {
    let output = Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("list")
        .output();

    match output {
        Ok(out) => {
            let command_output = String::from_utf8_lossy(&out.stdout);
            let raw_snapshots: Vec<String> = command_output
                .split('\n')
                .map(String::from)
                .filter(|s| s != "")
                .collect();
            let mut formatted_snapshot_name: Vec<String> = Vec::new();

            for snapshot in &raw_snapshots {
                formatted_snapshot_name.push(format_snapshot_name(snapshot.to_string()));
            }

            let mut snapshots: Vec<String> = Vec::new();

            for snapshot in &raw_snapshots {
                snapshots.push(extract_snapshot_name(snapshot.to_string()));
            }

            (formatted_snapshot_name, snapshots)
        }
        Err(..) => (
            vec![String::from("NO SNAPSHOT FOUND")],
            vec![String::from("NO SNAPSHOT FOUND")],
        ),
    }
}

fn running_name_mapper(vm_name: &str) -> String {
    let re = Regex::new(r#"^"(.*)""#).unwrap();

    match re.captures(vm_name) {
        Some(n) => match n.get(1) {
            Some(m) => String::from(m.as_str()),
            None => String::from(""),
        },
        None => String::from(""),
    }
}

pub fn get_running_vm_list() -> Vec<String> {
    let output = Command::new("VBoxManage")
        .arg("list")
        .arg("runningvms")
        .output();

    match output {
        Ok(out) => {
            let command_output = String::from_utf8_lossy(&out.stdout);

            if command_output != "" {
                command_output
                    .split('\n')
                    .map(running_name_mapper)
                    .filter(|s| s != "")
                    .collect()
            } else {
                vec!["No vms are currently running".to_string()]
            }
        }
        Err(..) => vec![String::from("CANNOT GET RUNNING VM")],
    }
}

#[cfg(test)]
mod tests {
    use crate::vm::vm_infos::{format_snapshot_name, running_name_mapper};

    use super::extract_snapshot_name;

    #[test]
    fn test_regex() {
        let name = running_name_mapper("\"TOTO\" {11111111111111111111111111111111111111}");
        assert_eq!("TOTO", name);
    }

    #[test]
    fn test_format_snapshot_name() {
        let name = format_snapshot_name(
            "    Name: toto (UUID: 123456-123456-111112222)".to_string(),
        );
        assert_eq!(name, "    - toto".to_string());

        let name = format_snapshot_name(
            "    Name: toto (UUID: 123456-123456-111112222) *".to_string(),
        );
        assert_eq!(name, "    - toto «".to_string());

        let name = format_snapshot_name(
            "    Name: toto (UUID: 123456-123456-111112222) *      ".to_string(),
        );
        assert_eq!(name, "    - toto «".to_string());
    }

    #[test]
    fn test_extract_snapshot_name() {
        let actual = extract_snapshot_name("    Name: toto (UUID: 1234567890)".to_string());
        assert_eq!(actual, "toto");

        let actual = extract_snapshot_name("    Name: toto (UUID: 1234567890) *".to_string());
        assert_eq!(actual, "toto");

        let actual = extract_snapshot_name("This machine does not have any snapshot".to_string());
        assert_eq!(actual, "This machine does not have any snapshot");
    }
}
