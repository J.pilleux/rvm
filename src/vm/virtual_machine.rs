use crate::components::stateful_list::StatefulList;
use super::vm_infos::get_snapshot_lists;

pub struct Vm {
    name: String,
    formatted_snapshot_list: Vec<String>,
    pub snapshot_stateful_list: StatefulList<String>,
}

impl Vm {
    pub fn new(
        name: &str,
        formatted_snapshot_list: Vec<String>,
        raw_snapshot_list: Vec<String>,
    ) -> Vm {
        Vm {
            name: String::from(name),
            formatted_snapshot_list,
            snapshot_stateful_list: StatefulList::with_items(raw_snapshot_list),
        }
    }

    pub fn no_vm_selected() -> Vec<String> {
        vec![String::from("No vm currently selected")]
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }

    pub fn get_formatted_snapshot_list(&self) -> Vec<String> {
        self.formatted_snapshot_list.clone()
    }

    pub fn get_snapshot_stateful_list(&self) -> &StatefulList<String> {
        &self.snapshot_stateful_list
    }

    pub fn refresh_snapshot_list(&mut self) {
        let (formatted_snapshot_list, raw_list) = get_snapshot_lists(&self.name);
        self.formatted_snapshot_list = formatted_snapshot_list;
        self.snapshot_stateful_list = StatefulList::with_items(raw_list);
    }
}
