use crate::components::{command_output::CommandOutput, menus::VMActionsMenus};


pub struct VmAction<'a> {
    name: String,
    description: String,
    action: &'a dyn Fn(&str, &str) -> CommandOutput,
    menu: VMActionsMenus,
}

impl<'a> VmAction<'a> {
    pub fn new(
        name: &str,
        description: &str,
        action: &'a dyn Fn(&str, &str) -> CommandOutput,
        menu: VMActionsMenus
    ) -> VmAction<'a> {
        VmAction {
            name: String::from(name),
            description: String::from(description),
            action,
            menu
        }
    }

    pub fn get_name(&self) -> &String {
        &self.name
    }

    pub fn get_description(&self) -> &String {
        &self.description
    }

    pub fn get_action(&self) -> &'a dyn Fn(&str, &str) -> CommandOutput {
        self.action
    }

    pub fn get_menu(&self) -> &VMActionsMenus {
        &self.menu
    }
}
