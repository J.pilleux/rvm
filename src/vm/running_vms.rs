use tui::{
    style::{Color, Style, Modifier},
    text::{Span, Spans},
    widgets::{Block, BorderType, Borders, ListItem, List},
};

fn get_running_vms() -> Vec<String> {
    vec![
        String::from("Toto"),
        String::from("Titi"),
        String::from("Tata"),
    ]
}

pub fn render_running_vms<'a>() -> List<'a> {
    let vms = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().fg(Color::White))
        .title("Running vms")
        .border_type(BorderType::Plain);

    let vm_list = get_running_vms();

    let items: Vec<_> = vm_list
        .iter()
        .map(|vm| ListItem::new(Spans::from(vec![Span::styled(vm.to_string(), Style::default())])))
        .collect();

    let list = List::new(items).block(vms).highlight_style(
        Style::default()
            .bg(Color::Yellow)
            .fg(Color::Black)
            .add_modifier(Modifier::BOLD),
    );

    list
}
