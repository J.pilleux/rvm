use std::process::Command;

use crate::components::command_output::CommandOutput;

pub struct SnapshotAction<'a> {
    name: String,
    description: String,
    action: &'a dyn Fn(&str, &str) -> CommandOutput,
}

pub fn get_snapshot_action_list() -> Vec<SnapshotAction<'static>> {
   vec![
       SnapshotAction::new("Take", "Take a snapshot with the current machine state", &snapshot_take),
       SnapshotAction::new("Restore", "Restore the selected machine to a given snapshot state", &snapshot_restore),
       SnapshotAction::new("Remove", "Remove a given snapshot", &snapshot_remove)
   ]
}

impl<'a> SnapshotAction<'a> {
    pub fn new(name: &str, description: &str, action: &'a dyn Fn(&str, &str) -> CommandOutput) -> SnapshotAction<'a> {
        SnapshotAction {
            name: name.to_string(),
            description: description.to_string(),
            action
        }
    }

    pub fn get_name(&self) -> &str {
        self.name.as_ref()
    }

    pub fn get_description(&self) -> &str {
        self.description.as_ref()
    }

    pub fn get_action(&self) -> &'a dyn Fn(&str, &str) -> CommandOutput {
        self.action
    }
}

fn snapshot_take(vm_name: &str, snapshot_name: &str) -> CommandOutput {
    let output = Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("take")
        .arg(snapshot_name)
        .output();

    match output {
        Ok(out) => CommandOutput::new(&out),
        Err(_) => CommandOutput::error_from_str(&format!("Cannot take snapshot « {} » for vm « {} »", snapshot_name, vm_name))
    }
}

fn snapshot_restore(vm_name: &str, snapshot_name: &str) -> CommandOutput {
    let output = Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("restore")
        .arg(snapshot_name)
        .output();

    match output {
        Ok(out) => CommandOutput::new(&out),
        Err(_) => CommandOutput::error_from_str(&format!("Cannot restore snapshot « {} » for vm « {} »", snapshot_name, vm_name))
    }
}


fn snapshot_remove(vm_name: &str, snapshot_name: &str) -> CommandOutput {
    let output = Command::new("VBoxManage")
        .arg("snapshot")
        .arg(vm_name)
        .arg("delete")
        .arg(snapshot_name)
        .output();

    match output {
        Ok(out) => CommandOutput::new(&out),
        Err(_) => CommandOutput::error_from_str(&format!("Cannot remove snapshot « {} » for vm « {} »", snapshot_name, vm_name))
    }
}
