use tui::{
    layout::Alignment,
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, Borders, List, ListItem, Paragraph},
};

pub fn make_stateful_list<'a>(str_list: &'a Vec<String>, list_name: &'a str) -> List<'a> {
    let list_items: Vec<ListItem> = str_list
        .iter()
        .map(|s| {
            let lines = vec![Spans::from(s.as_ref())];
            ListItem::new(lines).style(Style::default().fg(Color::White).bg(Color::Black))
        })
        .collect();

    List::new(list_items)
        .block(Block::default().borders(Borders::ALL).title(list_name))
        .highlight_style(
            Style::default()
                .bg(Color::Magenta)
                .fg(Color::Black)
                .add_modifier(Modifier::BOLD),
        )
}

pub fn make_list<'a>(list_content: &'a Vec<String>, list_name: &'a str) -> Paragraph<'a> {
    let paragraph_content: Vec<Spans> = list_content
        .iter()
        .map(|s| Spans::from(s.as_ref()))
        .collect();

    Paragraph::new(paragraph_content.clone())
        .style(Style::default().bg(Color::Black).fg(Color::White))
        .block(
            Block::default()
                .borders(Borders::ALL)
                .style(Style::default().bg(Color::Black).fg(Color::White))
                .title(Span::styled(
                    list_name,
                    Style::default().add_modifier(Modifier::BOLD),
                )),
        )
        .alignment(Alignment::Left)
}

pub fn make_paragraph<'a>(
    text: &'a str,
    block_name: &'a str,
    alignment: Alignment,
) -> Paragraph<'a> {
    let description_block = Block::default()
        .borders(Borders::ALL)
        .style(Style::default().bg(Color::Black).fg(Color::White))
        .title(Span::styled(
            block_name,
            Style::default().add_modifier(Modifier::BOLD),
        ));

    Paragraph::new(text)
        .style(Style::default().bg(Color::Black).fg(Color::White))
        .block(description_block)
        .alignment(alignment)
}

pub fn make_prompt<'a>(prompt_input: &'a str, prompt_name: &'a str) -> Paragraph<'a> {
    Paragraph::new(prompt_input)
        .style(Style::default().fg(Color::Yellow))
        .block(Block::default().borders(Borders::ALL).title(prompt_name))
}
