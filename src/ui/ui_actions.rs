use super::ui_prelude::*;
use super::utils::{make_paragraph, make_prompt};
use super::{
    ui_snapshots::{
        render_current_vm_snapshot_list_choice, render_snapshot_action_list,
        render_snapshot_description, render_snapshot_take,
    },
    utils::make_stateful_list,
};
use crate::components::menus::{SnapshotActionMenus, SnapshotRemoveMenus, VMActionsMenus};

pub fn render_vm_actions<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let actions_chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(25), Constraint::Percentage(75)].as_ref())
        .split(chunks);

    let action_info_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(10), Constraint::Percentage(90)].as_ref())
        .split(actions_chunks[1]);

    match app.get_vm_actions_menu() {
        VMActionsMenus::Main => {
            render_action_list(frame, app, actions_chunks[0]);
            render_action_description(frame, app, action_info_chunks[0]);
            render_action_output(frame, app, action_info_chunks[1]);
        }
        VMActionsMenus::Remove => {
            render_remove_list(frame, app, actions_chunks[0]);
            render_action_description(frame, app, action_info_chunks[0]);
            render_action_output(frame, app, action_info_chunks[1]);
        }
        VMActionsMenus::Clone => {
            render_clone(frame, app, actions_chunks[0]);
            render_action_description(frame, app, action_info_chunks[0]);
            render_action_output(frame, app, action_info_chunks[1]);
        }
        VMActionsMenus::Snapshots => match app.get_snapshot_action_menu() {
            SnapshotActionMenus::Main => {
                render_snapshot_action_list(frame, app, actions_chunks[0]);
                render_snapshot_description(frame, app, action_info_chunks[0]);
                render_action_output(frame, app, action_info_chunks[1]);
            }
            SnapshotActionMenus::Take => {
                render_snapshot_take(frame, app, actions_chunks[0]);
                render_snapshot_description(frame, app, action_info_chunks[0]);
                render_action_output(frame, app, action_info_chunks[1]);
            }
            SnapshotActionMenus::Restore => {
                render_current_vm_snapshot_list_choice(frame, app, actions_chunks[0]);
                render_snapshot_description(frame, app, action_info_chunks[0]);
                render_action_output(frame, app, action_info_chunks[1]);
            }
            SnapshotActionMenus::Remove => match app.get_snapshot_remove_memu() {
                SnapshotRemoveMenus::SelectSnapshot => {
                    render_current_vm_snapshot_list_choice(frame, app, actions_chunks[0]);
                    render_snapshot_description(frame, app, action_info_chunks[0]);
                    render_action_output(frame, app, action_info_chunks[1]);
                }
                SnapshotRemoveMenus::AskConfirmation => {
                    render_remove_list(frame, app, actions_chunks[0]);
                    render_snapshot_description(frame, app, action_info_chunks[0]);
                    render_action_output(frame, app, action_info_chunks[1]);
                }
            },
        },
    }
}

pub fn render_action_list<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let action_names: Vec<String> = app
        .vm_actions
        .items
        .iter()
        .map(|a| String::from(a.get_name()))
        .collect();

    let items = make_stateful_list(&action_names, "VM actions");
    frame.render_stateful_widget(items, chunks, &mut app.vm_actions.state);
}

fn render_remove_list<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let items = make_stateful_list(
        &app.remove_menu_list.items,
        "Do you really want to remove this ?",
    );
    frame.render_stateful_widget(items, chunks, &mut app.remove_menu_list.state);
}

fn render_action_description<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let current_action = app.vm_actions.get_current_mut_item();

    let description = match current_action {
        Some(desc) => desc.get_description(),
        None => "No action selected",
    };

    let description_paragraph = make_paragraph(description, "Description", Alignment::Center);
    frame.render_widget(description_paragraph, chunks);
}

fn render_action_output<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let command_output = String::from(&app.get_command_output());
    let output_paragraph = make_paragraph(&command_output , "Output", Alignment::Left);
    frame.render_widget(output_paragraph, chunks);
}

fn render_clone<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let clone_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(10), Constraint::Percentage(90)].as_ref())
        .split(chunks);

    let input_content = String::from(app.get_input_field().get_value());
    let clone_prompt = make_prompt(&input_content.as_ref(), "Enter a name");
    frame.render_widget(clone_prompt, clone_chunks[0]);

    let instruction_text = r#"Type a new name for the clone
Press Enter once its done
Press Escape to abort"#;

    let instructions = make_paragraph(instruction_text.trim(), "Instructions", Alignment::Left);

    frame.render_widget(instructions, clone_chunks[1]);

    frame.set_cursor(
        clone_chunks[0].x + app.get_input_field().len() as u16 + 1,
        clone_chunks[0].y + 1,
    );
}
