use std::process;

use super::{
    ui_prelude::*,
    utils::{make_paragraph, make_prompt},
};

pub fn render_snapshot_action_list<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let action_names: Vec<String> = app
        .snapshot_menu_list
        .items
        .iter()
        .map(|a| a.get_name().to_string())
        .collect();
    let items = make_stateful_list(&action_names, "Snapshot actions");
    frame.render_stateful_widget(items, chunks, &mut app.snapshot_menu_list.state);
}

pub fn render_snapshot_description<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let current_action = app.snapshot_menu_list.get_current_mut_item();

    let description = match current_action {
        Some(desc) => desc.get_description(),
        None => "No snapshot action selected",
    };

    let description_paragraph = make_paragraph(description, "Description", Alignment::Center);
    frame.render_widget(description_paragraph, chunks);
}

pub fn render_snapshot_take<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let take_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(10), Constraint::Percentage(90)].as_ref())
        .split(chunks);

    let input_value = String::from(app.get_input_field().get_value().to_string());
    let take_snapshot_prompt = make_prompt(&input_value, "Enter a snapshot name");

    frame.render_widget(take_snapshot_prompt, take_chunks[0]);

    frame.set_cursor(
        take_chunks[0].x + app.get_input_field().len() as u16 + 1,
        take_chunks[0].y + 1,
    );
}

pub fn render_current_vm_snapshot_list_choice<B: Backend>(
    frame: &mut Frame<B>,
    app: &mut App,
    chunks: Rect,
) {
    let current_vm_snapshot_stateful_list = app.vm_list.get_current_mut_item();

    match current_vm_snapshot_stateful_list {
        Some(current_vm) => {
            let items =
                make_stateful_list(&current_vm.snapshot_stateful_list.items, "Snapshot actions");

            frame.render_stateful_widget(
                items,
                chunks,
                &mut current_vm.snapshot_stateful_list.state,
            );
        }
        None => {
            eprintln!("NO VM SELECTED FOR SNAPSHOT DISPLAY");
            process::exit(1);
        }
    };
}
