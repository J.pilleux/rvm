use super::ui_actions::render_vm_actions;
use super::ui_prelude::*;
use super::utils::{make_list, make_paragraph};
use crate::components::menus::Menus;

pub fn ui<B: Backend>(frame: &mut Frame<B>, app: &mut App) {
    let main_chunks = Layout::default()
        .direction(Direction::Vertical)
        .margin(2)
        .constraints([Constraint::Percentage(5), Constraint::Percentage(95)])
        .split(frame.size());

    render_app_title(frame, main_chunks[0]);

    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(15), Constraint::Percentage(85)].as_ref())
        .split(main_chunks[1]);

    let left_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(30), Constraint::Percentage(70)].as_ref())
        .split(chunks[0]);

    render_running_vm(frame, app, left_chunks[0]);
    render_commands(frame, app, left_chunks[1]);

    let right_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(60), Constraint::Percentage(40)].as_ref())
        .split(chunks[1]);

    match app.get_current_menu() {
        Menus::Main => {
            render_vm_list(frame, app, right_chunks[0]);
            render_vm_snapshot_list(frame, app, right_chunks[1]);
        }
        Menus::VMActions => {
            render_vm_actions(frame, app, right_chunks[0]);
            render_vm_snapshot_list(frame, app, right_chunks[1]);
        }
    }
}

fn render_app_title<B: Backend>(frame: &mut Frame<B>, chunks: Rect) {
    let app_title = make_paragraph(
        "Rust VirtualBox terminal application",
        "Title",
        Alignment::Center,
    );
    frame.render_widget(app_title, chunks);
}

fn render_running_vm<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let content = app.get_running_vm();
    let running_vms = make_list(&content, "Running vms");
    frame.render_widget(running_vms, chunks);
}

fn render_commands<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let content = app.get_normal_mode_keys_description();
    let commands = make_paragraph(&content, "Commands", Alignment::Left);
    frame.render_widget(commands, chunks);
}

fn render_vm_list<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let all_vms: Vec<String> = app
        .vm_list
        .items
        .iter()
        .map(|i| i.get_name().to_string())
        .collect();

    let items = make_stateful_list(&all_vms, "All vms");

    frame.render_stateful_widget(items, chunks, &mut app.vm_list.state);
}

fn render_vm_snapshot_list<B: Backend>(frame: &mut Frame<B>, app: &mut App, chunks: Rect) {
    let snapshot_chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints([Constraint::Percentage(15), Constraint::Percentage(85)].as_ref())
        .split(chunks);

    let current_vm = &app.vm_list.get_current_mut_item();

    let vm_name = match current_vm {
        Some(vm) => vm.get_name(),
        None => "Please select a vm",
    };

    let snapshot_title = make_paragraph(vm_name, "VM name", Alignment::Center);
    frame.render_widget(snapshot_title, snapshot_chunks[0]);

    let vm_snapshots = match current_vm {
        Some(vm) => vm.get_formatted_snapshot_list(),
        None => vec![String::from("No VM currently selected")],
    };

    let snapshot_text = vm_snapshots.join("\n");
    let snapshot_list = make_paragraph(&snapshot_text, "Snapshots", Alignment::Left);

    frame.render_widget(snapshot_list, snapshot_chunks[1]);
}
