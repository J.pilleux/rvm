pub enum Menus {
    Main,
    VMActions,
}

pub enum VMActionsMenus {
    Main,
    Remove,
    Clone,
    Snapshots,
}

pub enum SnapshotActionMenus {
    Main,
    Take,
    Restore,
    Remove
}

pub enum SnapshotRemoveMenus {
    SelectSnapshot,
    AskConfirmation
}
