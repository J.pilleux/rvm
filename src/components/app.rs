use crossterm::event::KeyCode;

use crate::{
    cfg::keys_handler::NormalModeKeysHandler,
    vm::{
        snapshot_action::{get_snapshot_action_list, SnapshotAction},
        virtual_machine::Vm,
        vm_action::VmAction,
        vm_commands::get_vm_actions,
        vm_infos::{get_running_vm_list, get_vm_list},
    },
};

use super::{
    input_field::InputField,
    menus::{Menus, SnapshotActionMenus, SnapshotRemoveMenus, VMActionsMenus},
    stateful_list::StatefulList,
};

pub enum InputMode {
    Normal,
    Editing,
}

pub struct App<'a> {
    vm_running: Vec<String>,

    pub vm_list: StatefulList<Vm>,
    pub vm_actions: StatefulList<VmAction<'a>>,
    pub remove_menu_list: StatefulList<String>,
    pub snapshot_menu_list: StatefulList<SnapshotAction<'a>>,

    current_menu: Menus,
    vm_actions_menu: VMActionsMenus,
    snapshot_action_menu: SnapshotActionMenus,
    snapshot_remove_menu: SnapshotRemoveMenus,

    input_mode: InputMode,
    normal_input_handler: NormalModeKeysHandler,

    command_output: String,
    input: InputField,
}

impl<'a> App<'a> {
    fn update_running_vm(&mut self) {
        self.vm_running = get_running_vm_list();
    }

    pub fn new(normal_key_handler: NormalModeKeysHandler) -> App<'a> {
        App {
            vm_running: Vec::new(),
            vm_list: StatefulList::with_items(get_vm_list()),
            vm_actions: StatefulList::with_items(get_vm_actions()),
            remove_menu_list: StatefulList::with_items(vec!["Yes".to_string(), "No".to_string()]),
            snapshot_menu_list: StatefulList::with_items(get_snapshot_action_list()),
            command_output: String::new(),

            input_mode: InputMode::Normal,
            normal_input_handler: normal_key_handler,

            current_menu: Menus::Main,
            vm_actions_menu: VMActionsMenus::Main,
            snapshot_action_menu: SnapshotActionMenus::Main,
            snapshot_remove_menu: SnapshotRemoveMenus::SelectSnapshot,

            input: InputField::new(),
        }
    }

    pub fn get_running_vm(&self) -> Vec<String> {
        self.vm_running.to_vec()
    }

    pub fn get_current_menu(&self) -> &Menus {
        &self.current_menu
    }

    pub fn get_vm_actions_menu(&self) -> &VMActionsMenus {
        &self.vm_actions_menu
    }

    pub fn get_input_mode(&self) -> &InputMode {
        &self.input_mode
    }

    pub fn get_snapshot_action_menu(&self) -> &SnapshotActionMenus {
        &self.snapshot_action_menu
    }

    pub fn get_snapshot_remove_memu(&self) -> &SnapshotRemoveMenus {
        &self.snapshot_remove_menu
    }

    pub fn get_normal_mode_keys_description(&self) -> &String {
        &self.normal_input_handler.get_key_description()
    }

    pub fn get_command_output(&self) -> String {
        String::from(&self.command_output)
    }

    fn set_command_output(&mut self, new_content: &str) {
        self.command_output = new_content.to_string();
    }

    fn update_command_output(&mut self, new_content: &str) {
        if self.command_output.is_empty() {
            self.command_output = new_content.to_string();
        } else {
            self.command_output = format!("{}\n{}", self.command_output, new_content);
        }
    }

    fn reset_command_output(&mut self) {
        self.command_output = String::new();
    }

    pub fn get_current_snapshot_stateful_list(&mut self) -> Option<&StatefulList<String>> {
        match self.vm_list.get_current_mut_item() {
            Some(vm) => return Some(vm.get_snapshot_stateful_list()),
            None => return None,
        }
    }

    pub fn get_input_field(&self) -> &InputField {
        &self.input
    }

    pub fn mut_input_field(&mut self) -> &mut InputField {
        &mut self.input
    }

    pub fn goto_normal_mode(&mut self) {
        self.input_mode = InputMode::Normal;
    }

    pub fn goto_edit_mode(&mut self) {
        self.input_mode = InputMode::Editing;
    }

    pub fn on_tick(&mut self) {
        self.update_running_vm();
    }

    pub fn handle_key_in_normal_mode(&mut self, key_code: KeyCode) -> bool {
        let mut should_quit = false;
        match self.normal_input_handler.convert_to_action(key_code) {
            Some(action) => match action {
                crate::cfg::keys_handler::NormalModeActions::Quit => should_quit = true,
                crate::cfg::keys_handler::NormalModeActions::Unselect => self.unselect_items(),
                crate::cfg::keys_handler::NormalModeActions::Next => self.next_item(),
                crate::cfg::keys_handler::NormalModeActions::Previous => self.previous_item(),
                crate::cfg::keys_handler::NormalModeActions::Enter => self.handle_enter_key(),
                crate::cfg::keys_handler::NormalModeActions::Back => self.handle_backspace_key(),
            },
            None => {}
        }

        should_quit
    }

    pub fn unselect_items(&mut self) {
        match self.current_menu {
            Menus::Main => self.vm_list.unselect(),
            Menus::VMActions => match self.vm_actions_menu {
                VMActionsMenus::Main => self.vm_actions.unselect(),
                VMActionsMenus::Remove => self.remove_menu_list.unselect(),
                VMActionsMenus::Clone => {}
                VMActionsMenus::Snapshots => match self.snapshot_action_menu {
                    SnapshotActionMenus::Main => self.snapshot_menu_list.next(),
                    SnapshotActionMenus::Take => {}
                    SnapshotActionMenus::Restore => self.unselect_snapshot_name(),
                    SnapshotActionMenus::Remove => match self.snapshot_remove_menu {
                        SnapshotRemoveMenus::SelectSnapshot => self.unselect_snapshot_name(),
                        SnapshotRemoveMenus::AskConfirmation => self.snapshot_menu_list.unselect(),
                    },
                },
            },
        }
    }

    fn unselect_snapshot_name(&mut self) {
        let current_vm_snapshot_stateful_list = self.vm_list.get_current_mut_item();
        match current_vm_snapshot_stateful_list {
            Some(current_vm) => current_vm.snapshot_stateful_list.unselect(),
            None => {}
        }
    }

    pub fn next_item(&mut self) {
        match self.current_menu {
            Menus::Main => self.vm_list.next(),
            Menus::VMActions => match self.vm_actions_menu {
                VMActionsMenus::Main => self.vm_actions.next(),
                VMActionsMenus::Remove => self.remove_menu_list.next(),
                VMActionsMenus::Clone => {}
                VMActionsMenus::Snapshots => match self.snapshot_action_menu {
                    SnapshotActionMenus::Main => self.snapshot_menu_list.next(),
                    SnapshotActionMenus::Take => {}
                    SnapshotActionMenus::Restore => self.next_snapshot_name(),
                    SnapshotActionMenus::Remove => match self.snapshot_remove_menu {
                        SnapshotRemoveMenus::SelectSnapshot => self.next_snapshot_name(),
                        SnapshotRemoveMenus::AskConfirmation => self.remove_menu_list.next(),
                    },
                },
            },
        }
    }

    fn next_snapshot_name(&mut self) {
        let current_vm_snapshot_stateful_list = self.vm_list.get_current_mut_item();
        match current_vm_snapshot_stateful_list {
            Some(current_vm) => current_vm.snapshot_stateful_list.next(),
            None => {}
        }
    }

    pub fn previous_item(&mut self) {
        match self.current_menu {
            Menus::Main => self.vm_list.previous(),
            Menus::VMActions => match self.vm_actions_menu {
                VMActionsMenus::Main => self.vm_actions.previous(),
                VMActionsMenus::Remove => self.remove_menu_list.previous(),
                VMActionsMenus::Clone => {}
                VMActionsMenus::Snapshots => match self.snapshot_action_menu {
                    SnapshotActionMenus::Main => self.snapshot_menu_list.previous(),
                    SnapshotActionMenus::Take => {}
                    SnapshotActionMenus::Restore => self.previous_snapshot_name(),
                    SnapshotActionMenus::Remove => match self.snapshot_remove_menu {
                        SnapshotRemoveMenus::SelectSnapshot => self.previous_snapshot_name(),
                        SnapshotRemoveMenus::AskConfirmation => self.remove_menu_list.previous(),
                    },
                },
            },
        }
    }

    fn previous_snapshot_name(&mut self) {
        let current_vm_snapshot_stateful_list = self.vm_list.get_current_mut_item();
        match current_vm_snapshot_stateful_list {
            Some(current_vm) => current_vm.snapshot_stateful_list.previous(),
            None => {}
        }
    }

    pub fn handle_enter_key(&mut self) {
        match self.current_menu {
            Menus::Main => self.goto_vm_actions(),
            Menus::VMActions => match self.vm_actions_menu {
                VMActionsMenus::Main => {
                    let action_menu = match self.vm_actions.get_current_mut_item() {
                        Some(action) => action.get_menu(),
                        None => &VMActionsMenus::Main,
                    };

                    match action_menu {
                        VMActionsMenus::Main => self.run_command(),
                        VMActionsMenus::Remove => self.vm_actions_menu = VMActionsMenus::Remove,
                        VMActionsMenus::Clone => {
                            self.vm_actions_menu = VMActionsMenus::Clone;
                            self.input_mode = InputMode::Editing;
                        }
                        VMActionsMenus::Snapshots => {
                            self.vm_actions_menu = VMActionsMenus::Snapshots
                        }
                    }
                }
                VMActionsMenus::Remove => {
                    let choice = match self.remove_menu_list.get_current_mut_item() {
                        Some(c) => c,
                        None => "No",
                    };

                    if choice == "Yes" {
                        self.run_command();
                        self.remove_menu_list.unselect();
                    }

                    self.vm_actions_menu = VMActionsMenus::Main;
                }
                VMActionsMenus::Clone => {
                    self.run_command();
                    self.vm_actions_menu = VMActionsMenus::Main;
                    self.input_mode = InputMode::Normal;
                }
                VMActionsMenus::Snapshots => self.handle_snapshot_menu_enter_key(),
            },
        }
    }

    fn goto_vm_actions(&mut self) {
        match self.vm_list.state.selected() {
            Some(_) => self.current_menu = Menus::VMActions,
            None => {
                self.current_menu = Menus::Main;
                self.vm_actions.unselect();
            }
        };
    }

    fn goto_main(&mut self) {
        self.current_menu = Menus::Main;
        self.vm_actions.unselect();
        self.reset_command_output();
    }

    pub fn handle_snapshot_menu_enter_key(&mut self) {
        match self.snapshot_action_menu {
            SnapshotActionMenus::Main => match self.snapshot_menu_list.get_current_mut_item() {
                Some(current_menu) => match current_menu.get_name() {
                    "Take" => {
                        self.snapshot_action_menu = SnapshotActionMenus::Take;
                        self.input_mode = InputMode::Editing;
                    }
                    "Restore" => self.snapshot_action_menu = SnapshotActionMenus::Restore,
                    "Remove" => self.snapshot_action_menu = SnapshotActionMenus::Remove,
                    _ => {}
                },
                None => {}
            },
            SnapshotActionMenus::Take => {
                self.run_snapshot_command();
                match self.vm_list.get_current_mut_item() {
                    Some(current_vm) => current_vm.refresh_snapshot_list(),
                    None => panic!("No vm selected to take a snapshot"), // This should not happen
                }
                self.input_mode = InputMode::Normal;
                self.snapshot_action_menu = SnapshotActionMenus::Main;
            }
            SnapshotActionMenus::Restore => {
                self.run_snapshot_restore_command();
                match self.vm_list.get_current_mut_item() {
                    Some(current_vm) => current_vm.refresh_snapshot_list(),
                    None => panic!("No vm selected to take a snapshot"), // This should not happen
                }
                self.snapshot_action_menu = SnapshotActionMenus::Main;
            }
            SnapshotActionMenus::Remove => match self.snapshot_remove_menu {
                SnapshotRemoveMenus::SelectSnapshot => {
                    self.snapshot_remove_menu = SnapshotRemoveMenus::AskConfirmation;
                }
                SnapshotRemoveMenus::AskConfirmation => {
                    let choice = match self.remove_menu_list.get_current_mut_item() {
                        Some(c) => c,
                        None => "No",
                    };

                    if choice == "Yes" {
                        self.run_snapshot_restore_command();
                        self.update_running_vm();
                        self.remove_menu_list.unselect();
                    }

                    self.snapshot_action_menu = SnapshotActionMenus::Main;
                    self.snapshot_remove_menu = SnapshotRemoveMenus::SelectSnapshot;
                }
            },
        }
    }

    pub fn handle_backspace_key(&mut self) {
        match self.current_menu {
            Menus::Main => {}
            Menus::VMActions => match self.vm_actions_menu {
                VMActionsMenus::Main => self.goto_main(),
                _ => {
                    self.vm_actions_menu = VMActionsMenus::Main;
                    self.snapshot_action_menu = SnapshotActionMenus::Main;
                    self.remove_menu_list.unselect();
                    self.snapshot_menu_list.unselect();
                    self.remove_menu_list.unselect();
                    self.reset_command_output();
                }
            },
        }
    }

    fn run_command(&mut self) {
        match self.vm_actions.get_current_mut_item() {
            Some(selected_action) => {
                let action = selected_action.get_action();
                let menu = selected_action.get_menu();
                //self.update_command_output(&format!("Running action : {}", selected_action.get_name()));

                match self.vm_list.get_current_mut_item() {
                    Some(selected_vm) => {
                        let vm_name = selected_vm.get_name();
                        let output = match menu {
                            VMActionsMenus::Clone => {
                                let new_name = String::from(&self.input.get_value());
                                self.input.reset();
                                action(vm_name, &new_name)
                            }
                            _ => action(vm_name, ""),
                        };

                        self.update_command_output(&output.to_string());
                    }
                    None => self.set_command_output("ERROR: No vm selected"),
                }
            }
            None => self.set_command_output("ERROR: No action selected"),
        }
    }

    fn run_snapshot_command(&mut self) {
        match self.snapshot_menu_list.get_current_mut_item() {
            Some(selected_action) => {
                let action_name = selected_action.get_name();
                let action = selected_action.get_action();
                self.command_output = format!("Running snapshot action : {}", action_name);

                match self.vm_list.get_current_mut_item() {
                    Some(selected_vm) => {
                        let vm_name = selected_vm.get_name();
                        let new_name = self.input.get_value();
                        self.input.reset();
                        let output = action(vm_name, &new_name);
                        self.update_command_output(&output.to_string());
                    }
                    None => self.set_command_output("ERROR: No vm selected"),
                }
            }
            None => self.set_command_output("ERROR: No action selected"),
        }
    }

    fn run_snapshot_restore_command(&mut self) {
        match self.snapshot_menu_list.get_current_mut_item() {
            Some(selected_action) => {
                let action_name = selected_action.get_name();
                let action = selected_action.get_action();
                self.command_output = format!("Running snapshot action : {}", action_name);

                match self.vm_list.get_current_mut_item() {
                    Some(selected_vm) => {
                        let vm_name = String::from(selected_vm.get_name());
                        match selected_vm.snapshot_stateful_list.get_current_item() {
                            Some(current_snapshot) => {
                                let output = action(&vm_name, &current_snapshot);
                                self.update_command_output(&output.to_string());
                            }
                            None => self.set_command_output("ERROR: No snapshot selected"),
                        }
                    }
                    None => self.set_command_output("ERROR: No vm selected"),
                }
            }
            None => self.set_command_output("ERROR: No action selected"),
        }
    }

    pub fn handle_esc_key(&mut self) {
        match self.current_menu {
            Menus::Main => {}
            Menus::VMActions => match self.vm_actions_menu {
                VMActionsMenus::Clone => {
                    self.input_mode = InputMode::Normal;
                    self.vm_actions_menu = VMActionsMenus::Main;
                }
                VMActionsMenus::Snapshots => match self.snapshot_action_menu {
                    SnapshotActionMenus::Take => {
                        self.input_mode = InputMode::Normal;
                        self.snapshot_action_menu = SnapshotActionMenus::Main;
                    }
                    _ => {}
                },
                _ => {}
            },
        }
    }
}
