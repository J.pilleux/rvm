use core::fmt;
use std::process::Output;

pub struct CommandOutput {
    stdout: String,
    stderr: String,
}

impl CommandOutput {
    pub fn new(output: &Output) -> CommandOutput {
        let stdout = String::from_utf8_lossy(&output.stdout).to_string();
        let stderr = String::from_utf8_lossy(&output.stderr).to_string();
        CommandOutput { stdout, stderr }
    }

    pub fn from_str(stdout: &str, stderr: &str) -> CommandOutput {
        CommandOutput {
            stdout: stdout.to_string(),
            stderr: stderr.to_string()
        }
    }

    pub fn error_from_str(error: &str) -> CommandOutput {
        CommandOutput {
            stdout: String::new(),
            stderr: error.to_string()
        }
    }

    pub fn stdout(&self) -> &str {
        self.stdout.as_ref()
    }

    pub fn stderr(&self) -> &str {
        self.stderr.as_ref()
    }

    pub fn has_command_fail(&self) -> bool {
        ! self.stderr.is_empty()
    }
}

impl fmt::Display for CommandOutput {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.has_command_fail() {
            write!(f, "{}", self.stderr)
        } else {
            write!(f, "{}", self.stdout)
        }
    }
}
