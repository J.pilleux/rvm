#[macro_use]
extern crate lazy_static;

pub mod main_menu;
pub mod components;
pub mod ui;
pub mod vm;
pub mod cfg;
