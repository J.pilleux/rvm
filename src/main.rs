use rvm::main_menu::run_main_menu;

extern crate rvm;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    run_main_menu()
}
