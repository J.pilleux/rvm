use std::{
    io, process,
    time::{Duration, Instant},
};

use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};

use tui::{
    backend::{Backend, CrosstermBackend},
    Terminal,
};

use crate::{
    cfg::keys_handler::NormalModeKeysHandler,
    components::app::{App, InputMode},
    ui::main_ui::ui,
};

#[allow(unused_must_use)]
#[allow(unreachable_code)]
pub fn run_main_menu() -> Result<(), Box<dyn std::error::Error>> {
    enable_raw_mode().expect("can run in raw mode");

    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture);
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // TODO : Put this in parameters
    let tick_rate = Duration::from_millis(200);
    let key_handler = NormalModeKeysHandler::new(
        vec!["q"],
        vec!["Left"],
        vec!["t", "Down"],
        vec!["s", "Up"],
        vec!["r", "Enter"],
        vec!["Backspace", "c"],
    );

    let handler = match key_handler {
        Ok(handler) => handler,
        Err(err) => {
            eprint!("{}", err);
            process::exit(1);
        }
    };

    let app = App::new(handler);
    let res = run_app(&mut terminal, app, tick_rate);

    disable_raw_mode().expect("Disable the terminal raw mode");

    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    );
    terminal.show_cursor().expect("Terminal show cursor");

    if let Err(err) = res {
        println!("{:?}", err);
    }

    Ok(())
}

fn handle_insert_input_mode(key_code: &KeyCode, app: &mut App) {
    match key_code {
        KeyCode::Enter => {
            app.handle_enter_key();
        }
        KeyCode::Char(c) => {
            app.mut_input_field().type_char(*c);
        }
        KeyCode::Backspace => {
            app.mut_input_field().erase();
        }
        KeyCode::Esc => {
            app.handle_esc_key();
        }
        _ => {}
    }
}

fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
    tick_rate: Duration,
) -> io::Result<()> {
    let mut last_tick = Instant::now();

    loop {
        terminal.draw(|f| ui(f, &mut app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));

        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                match app.get_input_mode() {
                    InputMode::Normal => {
                        let should_quit = app.handle_key_in_normal_mode(key.code);
                        if should_quit {
                            return Ok(());
                        };
                    }
                    InputMode::Editing => handle_insert_input_mode(&key.code, &mut app),
                }
            }
        }

        if last_tick.elapsed() >= tick_rate {
            app.on_tick();
            last_tick = Instant::now();
        }
    }
}
